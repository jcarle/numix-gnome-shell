## A GNOME Shell theme that matches the Numix GTK+ theme.

Based on this theme by Marathon2112:
https://www.gnome-look.org/p/1012907/


### Installation instructions:

* Make sure the User Themes extension is enabled in gnome-tweaks
* Create a .theme directory in your home folder:
  `mkdir ~/.themes`
* Download and extract the archive into the destination folder:
 `tar -xzvf ARCHIVE_NAME.tar.gz ~/.themes`
* Choose your theme using gnome-tweaks

